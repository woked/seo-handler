<?php
include './bootstrap.php';
include MAIN_PATH.'/data_save.php';
$data = $seo_data->data;
$seo = new seo_handler;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>seo handler 2.0</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
</head>
<body>
  <nav class="row">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo">Seo handler 2.0</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="/seo_handler/">Добавить</a></li>
        <li><a href="/seo_handler/list_page.php">Список страниц</a><li>
        <li><a href="/seo_handler/global_settings.php">Глобальные настройки</a><li>
      </ul>
    </div>
  </nav>

  <div class="main-wrap">

    <?php if (!isset($_GET['page'])): ?>
    <div class="container">
        <div class="col s12">
          <ul class="collection">
            <?php foreach ($data as $key => $value): ?>
            <li class="collection-item"><a href="?page=<?= $key; ?>"><?= $value['url']; ?>
              <?php if (!empty($value['alias'])) echo ' -> '.$value['alias']; ?>
            </a></li>
            <?php endforeach; ?>
          </ul>
        </div>
    </div>
    <?php endif; ?>



    <?php 
      if (isset($_GET['page'])):

      $data = $data[$_GET['page']];
    ?>
    <div class="container">
        <div class="col s12">
          <div class="row">
            <div class="col s4">
              <a href="/seo_handler/list_page.php" style="vertical-align: top; float: left;"><i class="material-icons" style="vertical-align: top;">chevron_left</i>назад</a>
              <a href="<?='//'.$_SERVER['SERVER_NAME'].$seo->correct_url($data['url'])?>" target="_blank" style="vertical-align: top; float: right;" title="Открыть страницу"><i class="material-icons">open_in_browser</i></a>
            </div>
          </div>
          <form class="col s12" action="data_save.php" method="POST">
            <input type="hidden" name="id" value="<?= $_GET['page'] ?>">
            <div class="row">
              <div class="input-field col s12">
                <input placeholder="" id="url" name="url" type="text" readonly="" value="<?= $data['url'] ?>">
                <label for="url">url</label>
              </div>
            </div>

            <div class="row">
              <div class="input-field col s12">
                <input placeholder="" id="title" name="title" type="text" value="<?= $data['title'] ?>">
                <label for="title">title</label>
              </div>
            </div>

            <div class="row">
              <div class="input-field col s12">
                <input placeholder="" id="description" name="description" type="text" value="<?= $data['description'] ?>">
                <label for="description">description</label>
              </div>
            </div>

            <div class="row">
              <div class="input-field col s12">
                <input placeholder="" id="h1" name="h1" type="text" value="<?= $data['h1'] ?>">
                <label for="h1">h1</label>
              </div>
            </div>

            <div class="row">
              <div class="input-field col s12">
                <input placeholder="" id="alias" name="alias" type="text" value="<?= $data['alias'] ?>">
                <label for="alias">alias</label>
              </div>
            </div>

            <ul class="collapsible" data-collapsible="accordion">
              <li>
              <div class="collapsible-header">Изменить контент</div>
              <div class="collapsible-body">
              <div class="row">
                <div class="input-field col s12">
                  <input placeholder="" id="selector" name="selector" type="text" value="<?= htmlspecialchars($data['selector']) ?>">
                  <label for="selector">Селектор</label>
                </div>
              </div>

              <div class="row">
                <div class="input-field col s12">
                  <textarea id="insertContent" name="insertContent" class="materialize-textarea" type="text"><?= htmlspecialchars($data['insertContent']) ?></textarea>
                  <label for="insertContent">Контент</label>
                </div>
              </div>
              </div>
              </li>
            </ul>

            <div class="row">
              <div class="input-field col s6">
                <button class="waves-effect waves-light btn" name="update" id="update">Изменить</button>
              </div>
              <div class="input-field col s6">
                <input type="submit" name="remove" id="remove" class="waves-effect waves-light btn red darken-1 right" value="Удалить">
              </div>
            </div>

          </form>
        </div>
    </div>
    <?php endif; ?>


  </div>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
  <script src="js/main.js"></script>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</body>
</html>