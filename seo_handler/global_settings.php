<?php
include './bootstrap.php';
include MAIN_PATH.'/data_save.php';
$data = $seo_data->data;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>seo handler 2.0</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
</head>
<body>
<nav class="row">
    <div class="nav-wrapper">
        <a href="#" class="brand-logo">Seo handler 2.0</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="/seo_handler/">Добавить</a></li>
            <li><a href="/seo_handler/list_page.php">Список страниц</a><li>
            <li><a href="/seo_handler/global_settings.php">Глобальные настройки</a><li>
        </ul>
    </div>
</nav>

<div class="main-wrap">

        <?php
        $data = $data['global'];
        ?>
        <div class="container">
            <div class="col s12">
                <form class="col s12" action="data_save.php" method="POST">
                    <input type="hidden" name="id" value="global">
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="" id="common_title" name="common_title" type="text" value="<?= $data['common_title'] ?>">
                            <label for="common_title">common title</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s6">
                            <button class="waves-effect waves-light btn" name="update_global" id="update_global">Изменить</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>


</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="js/main.js"></script>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</body>
</html>