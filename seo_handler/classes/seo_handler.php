<?php
	class seo_handler {
		
		public $content				= null;
		
		private $url				= null;
		private $current_url		= null;
		private $alias				= null;
		private $redirects			= array();
		private $common_title		= null;
		private $title_separator	= null;
		private $titles				= array();
		private $descriptions		= array();
		private $keywords			= array();
		private $h1s				= array();
		private $https 				= null;
		private $host 				= null;
		private $links 				= array();

		private $selector			= null;
		private $insertContent		= null;
		

		public function __construct() {

			$this->current_url($_SERVER['REQUEST_URI']);

		}

		public function start() {

			if (!isset($_GET['referrer_seo']) && $this->alias !== NULL) {
				$this->check_alias();
			}

			ob_start();

			// $this->runRedirect();
			// $this->changeRequestURI();
			
		}

		//new
		//

		public function set_data($data)
		{

			foreach ($data as $key => $value) {

                if ($this->current_url == $this->correct_url($value['url']) || $this->current_url == $this->correct_url($value['alias'])
                    || $this->current_url == $this->correct_url($value['url']) . "?referrer_seo=1/") {
                    $this->url($this->correct_url($value['url']));
                    $this->set_alias($this->correct_url($value['alias']));
                    $this->set_title($value['title']);
                    $this->set_description($value['description']);
                    $this->set_h1($value['h1']);
                    $this->set_selector($value['selector']);
                    $this->set_insertContent($value['insertContent']);
                }

                if ($key == 'global') {
                    $this->set_common_title($value['common_title']);
                }

				$this->links[$value['url']] = $value['alias'];

			}			

		}

		public function correct_url($url) {

			if (!empty($url)) {
				if ('/' != substr($url, 0, 1)) {
					$url = '/'.$url;
				}
				if ('/' != substr($url, -1)) {
					$url = $url.'/';
				}
			}

			return $url;

		}

		public function current_url($url) {

			$this->current_url = $this->correct_url($url);

		}


		public function check_alias() {

			// foreach ($this->alias as $from => $to) {

				if ($this->current_url == $this->alias) {
					$this->get_content($this->get_host($this->url));
				} elseif ($this->current_url == $this->url && !empty($this->alias)) {
					$this->runAlias();
				}

			// }

		}


		public function is_https() {

			$this->https = !empty($_SERVER['HTTPS']) && 'off' !== strtolower($_SERVER['HTTPS']);
			if ($this->https) {
				return 'https://';
			} else {
				return 'http://';
			}
			
		}


		public function set_alias($alias) {

			if (!empty($alias)) {
				$this->alias = $alias;
			}

		}


		public function get_host($url) {
			$domain = str_replace('/', '', $_SERVER['SERVER_NAME']);
			$this->host = $this->is_https().$domain.$url;
			return $this->host;

		}


		public function get_content($url) {

			$this->content = file_get_contents($url.'?referrer_seo=1');
			echo $this->content;
			die();

		}
			
		//\new

		public function finish() {
			$this->content = ob_get_clean();
			// $this->obfuscate();
			$this->changeTitlesKeywordsDescriptionsH1();
			$this->addCommonTitle();
			$this->changeRedirectsLinks();
			$this->removeCyclicLinks();
			if (!empty($this->selector) && !empty($this->insertContent)) {
				$this->insertContent();
			}
			$this->output();
		}
		
		
		public function url($url) {
			
			$this->url = $this->correct_url($url);
			
		}
		
		
		public function set_redirect($url, $do_change = true) {
			
			$this->redirects[$this->url] = array(
				'url'		=> $url,
				'do_change'	=> (bool)$do_change
			);
			
		}
		
		
		public function set_common_title($common_title) {
			
			if (!empty($common_title)) {
				$this->common_title = $common_title;
			}
			
		}
		
		
		public function set_title($title) {
			
			if (!empty($title)) {
				$this->titles[$this->url] = $title;
			}
			
		}


		public function set_selector($selector) {

			if (!empty($selector)) {
				$this->selector = $selector;
			}
			
		}

		public function set_insertContent($insertContent) {

			if (!empty($insertContent)) {
				$this->insertContent = $insertContent;
			}
			
		}
		
		public function set_description($description) {
			
			$this->descriptions[$this->url] = $description;
			
		}
		
		
		public function set_keywords($keywords) {
			
			$this->keywords[$this->url] = $keywords;
			
		}
		
		
		public function set_h1($h1) {
			
			if (!empty($this->url)) {
				$this->h1s[$this->url]['value'] = trim($h1);
			}
			
		}
		
		
		public function set_h1_position($h1_position) {
			
			if (!empty($this->url)) {
				$this->h1s[$this->url]['position'] = (int)$h1_position;
			}
			
		}
		
		
		// public function set_logo_title_text($logo_title_text) {
			
			// if (!empty($logo_title_text)) {
				// $this->logo_title = $logo_title_text;
			// }
			
		// }
		
		
		public function output() {
			
			echo $this->content;
			
		}
		
		
		public function getRealLink($url) {
			
			if (!empty($this->redirects)) {
				foreach ($this->redirects as $from => $to) {
					if ($from == $url) {
						return $to['url'];
					}
				}
			}
			
			return $url;
			
		}
		
		
		private function changeRedirectsLinks() {
			
			if (empty($this->links)) {
				return false;
			}
			
			foreach ($this->links as $from => $to) {
				if (!empty($to)) {
					$this->content = preg_replace('#href="'.$from.'?"#', 'href="'.$to.'"', $this->content);
				}
			}
			
		}
		
		
		private function obfuscate() {
			
			$this->content	= preg_replace('/\s+/', ' ',  $this->content);
			$this->content	= str_replace('> <',    '><', $this->content);
			
		}

		private function insertContent() {
            $pq = phpQuery::newDocument($this->content);
            $pq->find($this->selector)->html($this->insertContent);
            $this->content = $pq->html();
        }

		private function changeTitlesKeywordsDescriptionsH1() {

			//kostilizaciya
            if ($this->alias !== NULL) {
                $this->current_url = str_replace('?referrer_seo=1/', '', $this->current_url);
            }

			if (!empty($this->titles[$this->current_url])) {
				$title = $this->titles[$this->current_url];
				$this->content = preg_replace('/<title>(.*?)<\/title>/i', '<title>'.$title.'</title>', $this->content);
			}
			
			if (!empty($this->keywords[$this->current_url])) {
				$keywords = $this->keywords[$this->current_url];
				if (preg_match('/\<meta[^>]+?name="keywords"[^>]+?\>/i', $this->content, $m)) {
					if (preg_match('/content="(.+?)"/i', $m[0], $mc)) {
						$this->content = str_replace($mc[1], $keywords, $this->content);
					}
				}
			}
			
			if (!empty($this->descriptions[$this->current_url])) {
				$description = $this->descriptions[$this->current_url];
				if (preg_match('/\<meta[^>]+?name="description"[^>]+?\>/i', $this->content, $m)) {
					if (preg_match('/content="(.+?)"/i', $m[0], $mc)) {
						$this->content = str_replace($mc[1], $description, $this->content);
					}
				}
			}
			
			$this->changeH1();
			
		}
		
		
		private function changeH1() {
			
			if (!empty($this->h1s[$this->current_url]) && !empty($this->h1s[$this->current_url]['value'])) {
				// определяем значение заменяемого заголовка h1 для обрабатываемой страницы
				$h1 = $this->h1s[$this->current_url]['value'];
				
				// определяем порядковый номер заменяемого заголовка h1 (нумерация с 1) для обрабатываемой страницы
				$h1_position = 0;
				// если порядковый номер заменяемого заголовка h1 для обрабатываемой страницы был установлен и он больше нуля, ...
				if (!empty($this->h1s[$this->current_url]['position']) && (int)$this->h1s[$this->current_url]['position'] > 0) {
					// приводим порядковый номер заменяемого заголовка h1 для обрабатываемой страницы к нумерации с 0
					$h1_position = (int)$this->h1s[$this->current_url]['position'] - 1;
				}
				
				if (preg_match_all('/\<h1(.*?)\>(.+?)\<\/h1\>/is', $this->content, $h1_matches)) {
					// находим позицию  в строке маски
					$h1_pointer		= mb_strpos($this->content, $h1_matches[0][$h1_position], 0, 'UTF-8');
					$h1_length		= mb_strlen($h1_matches[0][$h1_position], 'UTF-8');
					$h1_replacement	= preg_replace('/\<h1(.*?)\>(.+?)\<\/h1\>/is', '<h1$1>'.$h1.'</h1>', $h1_matches[0][$h1_position]);
					
					// заменяем подстроку в строке маски соответствующей подстрокой из строки с номером телефона
					// с ограничением по длине, равным количеству символов в текущей (обрабатываемой) подмаске
					$this->content = $this->mb_substr_replace($this->content, $h1_replacement, $h1_pointer, $h1_length, 'UTF-8');
				};
				
			}
			
		}
		
		
		private function addCommonTitle() {
			
			$separator = !empty($this->title_separator)		? $this->title_separator		: ' | ';
			
			if (!empty($this->common_title)) {
				$this->content = preg_replace('/\<title\>(.+?)\<\/title\>/', '<title>$1'.$separator.$this->common_title.'</title>', $this->content);
			}
			
		}
		
		
		private function removeCyclicLinks() {
			
			// парсим все ссылки текущего документа
			if (preg_match_all('/\<a[^>]*href\=\"([^"]+)\"[^>]*\>.+?\<\/a\>/is', $this->content, $links_matches)) {
				if (!empty($links_matches)) {
					// определяем шаблон для сравнения текущей (обрабатываемой) ссылки и $_SERVER['REQUEST_URI'] регулярным выражением
					$uri_pattern = '/^\/?([a-z0-9_\-\.]+)\/?$/i';
					
					foreach ($links_matches[1] as $href) {
						// если адрес текущей (обрабатываемой) ссылки не пустой, ...
						if (!empty($href)) {
							// определяем порядковый номер первого символа вхождения строки, содержащей адреса ссылки
							$href_pointer	= mb_strpos($this->content, 'href="'.$href.'"', 0, 'UTF-8') + 6;
							
							// определяем количество символов (длину строки) в адресе текущей (обрабатываемой) ссылки
							$href_length	= mb_strlen($href, 'UTF-8');
							
							// если текущая (обрабатываемая) ссылка является "циклической", ...
							if (preg_replace($uri_pattern, '$1', $href) == preg_replace($uri_pattern, '$1', $this->current_url)) {
								// изменяем адрес циклической ссылки
								$this->content = $this->mb_substr_replace($this->content, 'javascript:void(0);', $href_pointer, $href_length, 'UTF-8');
							}
						}
					}
				}
			}
			
		}
		
		
		private function runAlias() {

			if (empty($this->alias)) {
				return false;
			}

			// foreach ($this->alias as $from => $to) {
				if ($this->current_url == $this->url) {
					header('Location: '.$this->alias, true, 301);
					die();
				}
			// }

		}

		private function runRedirect() {
			
			if (empty($this->redirects)) {
				return false;
			}
			
			foreach ($this->redirects as $from => $to) {
				if ($this->current_url === $from) {
					header('HTTP/1.0 301 Moved Permanently');
					header('Location: '.$to['url']);
					die();
				}
			}
			
		}
		
		
		private function changeRequestURI() {

			if (empty($this->redirects)) {
				return false;
			}
			
			foreach ($this->redirects as $from => $to) {
				if ($this->current_url === $to['url'] && $to['do_change']) {
					$this->current_url = $from;
					return true;
				}
			}
			
			return true;
			
		}
		
		
		// if (function_exists('mb_substr_replace') === false) {
			
			function mb_substr_replace($string, $replacement, $start, $length = null, $encoding = null) {
				
				if (extension_loaded('mbstring') === true) {
					$string_length = (is_null($encoding) === true)		? mb_strlen($string)		: mb_strlen($string, $encoding);
					
					if ($start < 0) {
						$start = max(0, $string_length + $start);
					} elseif ($start > $string_length) {
						$start = $string_length;
					}
					
					if ($length < 0) {
						$length = max(0, $string_length - $start + $length);
					} elseif ((is_null($length) === true) || ($length > $string_length)) {
						$length = $string_length;
					}
					
					if (($start + $length) > $string_length) {
						$length = $string_length - $start;
					}
					
					if (is_null($encoding) === true) {
						return mb_substr($string, 0, $start).$replacement.mb_substr($string, $start + $length, $string_length - $start - $length);
					}
					
					return mb_substr($string, 0, $start, $encoding).$replacement.mb_substr($string, $start + $length, $string_length - $start - $length, $encoding);
				}
				
				return (is_null($length) === true)		? substr_replace($string, $replacement, $start)
														: substr_replace($string, $replacement, $start, $length);
				
			}
			
		// }
		
	}
?>