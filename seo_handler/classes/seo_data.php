<?php
class seo_data {

	public $data = array();
	public $file = null;
	public $path = '';

	private $errors = array();


	public function __construct() {
		$this->path = $_SERVER['DOCUMENT_ROOT'].'/seo_handler/data_test.json';
		$this->get_data();
	}


	public function get_data() {

		$this->file = file_get_contents($this->path);
		$this->data = json_decode($this->file, TRUE);

	}


	public function remove_data($needle) {

		foreach ($this->data as $key => $value) {
			if ($value['url'] == $needle) {
				unset($this->data[$key]);
			}
		}

	}


	public function append_data($seo_data) {

		if (!$this->check_exists_data($seo_data)) {
			$this->data[] = $seo_data;			
		}

	}


	public function check_exists_data($seo_data) {

		foreach ($this->data as $key => $value) {
			if (in_array($seo_data['url'], $value)) {
				$errors[] = 'Не удалось добавить. Такая страница уже есть.';
				return true;
			}
		}

		return false;

	}

	public function update_data($seo_data, $id = null) {

		if ($id === null) {

			foreach ($this->data as $key => &$value) {
				if ($value['url'] == $seo_data['url']) {
					$value = $seo_data;
				}
			}

		} else {

			foreach ($this->data as $key => &$value) {
				if ($key == $id) {
					$value = $seo_data;
				}
			}

		}

	}


	public function save() {
		
		if (!empty($this->data)) {
			$res = file_put_contents($this->path, json_encode($this->data));
		}
		
		if ($res !== false) {
			return true;
		}

		return false;

	}

}