<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/seo_handler/classes/seo_data.php');
$seo_data = new seo_data;
$seo_data->get_data();

if (!empty($_POST['save'])) {
	
	$data = array(
		'title' => $_POST['title'],
		'description' => $_POST['description'],
		'h1' => $_POST['h1'],
		'url' => $_POST['url'],
		'alias' => $_POST['alias'],
		'selector' => $_POST['selector'],
		'insertContent' => $_POST['insertContent']
	);

	$seo_data->append_data($data);
	$seo_data->save();

} elseif (!empty($_POST['remove'])){
	$data = array(
		'title' => $_POST['title'],
		'description' => $_POST['description'],
		'h1' => $_POST['h1'],
		'url' => $_POST['url'],
		'alias' => $_POST['alias']
	);
	
	$seo_data->remove_data($data['url']);
	$seo_data->save();

} elseif (!empty($_POST['update'])) {
	$id = $_POST['id'];
	$data = array(
		'title' => $_POST['title'],
		'description' => $_POST['description'],
		'h1' => $_POST['h1'],
		'url' => $_POST['url'],
		'alias' => $_POST['alias'],
		'selector' => $_POST['selector'],
		'insertContent' => $_POST['insertContent']
	);

	$seo_data->update_data($data, $id);
	$seo_data->save();
} elseif (!empty($_POST['update_global'])) {
    $id = $_POST['id'];
    $data = array(
        'common_title' => $_POST['common_title']
    );

    $seo_data->update_data($data, $id);
    $seo_data->save();
}