$(document).ready(function() {
  $('#save').on('click', function(e) {
    var $form = $('form');
    $.ajax({
      type: $form.attr('method'),
      url: $form.attr('action'),
      data: $form.serialize()+'&save=1'
    }).done(function() {
      alert('success');
    }).fail(function() {
      alert('fail');
    });
    e.preventDefault(); 
  });

  $('#update').on('click', function(e) {
    var $form = $('form');
    $.ajax({
      type: $form.attr('method'),
      url: $form.attr('action'),
      data: $form.serialize()+'&update=1'
    }).done(function() {
      alert('success');
    }).fail(function() {
      alert('fail');
    });
    e.preventDefault(); 
  });

  $('#remove').on('click', function(e) {
    var $form = $('form');
    $.ajax({
      type: $form.attr('method'),
      url: $form.attr('action'),
      data: $form.serialize()+'&remove=1'
    }).done(function() {
      alert('success');
    }).fail(function() {
      alert('fail');
    });
    e.preventDefault(); 
  });

  $('.collapsible').collapsible();


    $('#update_global').on('click', function(e) {
        var $form = $('form');
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize()+'&update_global=1'
        }).done(function() {
            alert('success');
        }).fail(function() {
            alert('fail');
        });
        e.preventDefault();
    });



});

