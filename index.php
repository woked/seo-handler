<?php

include 'seo_handler/bootstrap.php';
include MAIN_PATH.'/data_save.php';

$seo = new seo_handler();
$seo->set_data($seo_data->data);
$seo->start();

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Стандартный title</title>
	<meta name="description" content="Стандартный description">
	<script src="https://cdn.jsdelivr.net/npm/vue"></script>
</head>
<body>
	<div id="seo_text">
		<h1>Стандартный h1</h1>
		<p>{{ title }}</p>
		<p>{{ description }}</p>
		<div class="content">
			<p>default content :))))))))))))</p>
		</div>
	</div>


	<script>
		var vm = new Vue({
			el: "#seo_text",
			data: {
				title: document.title,
				description: document.querySelector('meta[name="description"]').getAttribute('content')
			}
		});
	</script>
</body>
</html>

<?php

$seo->finish();

?>